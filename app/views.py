from flask import render_template, redirect, url_for, Blueprint
from werkzeug.utils import secure_filename
from flask import request, current_app as app
from .forms import CodeSnippetForm, CodeFileForm, BlogPostForm
from .scripts.create_markdown import create_markdown
from .scripts.commit_to_git import commit_to_git
from .scripts.search_files import search_matching_files
import os
import fnmatch

main = Blueprint('main', __name__)

@main.route('/')
def index():
    return render_template('index.html')

@main.route('/share_code', methods=['GET', 'POST'])
def share_code():
    return render_template('share_code.html')

@main.route('/share_code_snippet', methods=['GET', 'POST'])
def share_code_snippet():
    form = CodeSnippetForm()
    if form.validate_on_submit():
        markdown_path = create_markdown(form.data, 'snippet')
        commit_to_git(form.data, markdown_path, 'snippet')
        return redirect(url_for('main.index'))
    return render_template('share_code_snippet.html', form=form)

@main.route('/share_code_file', methods=['GET', 'POST'])
def share_code_file():
    form = CodeFileForm()
    if form.validate_on_submit():
        filename = secure_filename(form.file.data.filename)
        file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        form.file.data.save(file_path)
        markdown_path = create_markdown(form.data, 'file', file_path)
        commit_to_git(form.data, markdown_path, 'file')
        return redirect(url_for('main.index'))
    return render_template('share_code_file.html', form=form)

@main.route('/share_blog_post', methods=['GET', 'POST'])
def share_blog_post():
    form = BlogPostForm()
    if form.validate_on_submit():
        markdown_path = create_markdown(form.data, 'blog')
        commit_to_git(form.data, markdown_path, 'blog')
        return redirect(url_for('main.index'))
    return render_template('share_blog_post.html', form=form)

@main.route('/search_code', methods=['GET', 'POST'])
def search_code():
    # You need to implement the code search functionality
    return render_template('search_code.html')

@main.route('/search_results', methods=['POST'])
def search_results():
    query = request.form['query']
    matches = search_matching_files(query)
    
    return render_template('search_results.html', matches=matches)