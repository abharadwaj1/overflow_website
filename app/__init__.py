from flask import Flask
from config import Config
from .views import main  # Import the blueprint
import os 

def create_app():
    app = Flask(__name__)
    app.register_blueprint(main)
    app.config.from_object(Config)
    app.config['UPLOAD_FOLDER'] = os.path.dirname(os.path.abspath(__file__)) + '/uploads/'
    return app
