from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, FileField, SelectField, SubmitField
from wtforms.validators import DataRequired
from flask_wtf.file import FileAllowed, FileRequired

class CodeSnippetForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    code = TextAreaField('Code', validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])
    language = SelectField('Language', choices=[('python', 'Python'), ('chimera', 'Chimera'), ('excel', 'Excel'), ('imagej', 'ImageJ')], validators=[DataRequired()])
    submit = SubmitField('Submit')

class CodeFileForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])
    file = FileField('Code File', validators=[FileRequired(), FileAllowed(['py', 'ipynb', 'xls', 'ijm'], 'Code files only!')])
    language = SelectField('Language', choices=[('python', 'Python'), ('chimera', 'Chimera'), ('excel', 'Excel'), ('imagej', 'ImageJ')], validators=[DataRequired()])
    submit = SubmitField('Submit')

class BlogPostForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    post = TextAreaField('Post', validators=[DataRequired()])
    image = FileField('Image', validators=[FileAllowed(['jpg', 'png'], 'Images only!')])
    submit = SubmitField('Submit')

class SearchForm(FlaskForm):
    keywords = StringField('Keywords', validators=[DataRequired()])
    submit = SubmitField('Search')

class CodeFileForms(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    code = FileField('Code', validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])
    language = SelectField('Language', choices=[('python', 'Python'), ('chimera', 'Chimera'), ('excel', 'Excel'), ('imagej', 'ImageJ')], validators=[DataRequired()])
    submit = SubmitField('Submit')