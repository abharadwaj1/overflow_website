from git import Repo
import os
import shutil

def commit_to_git(data, markdown_path, type):
    repo = Repo('/home/abharadwaj1/dev/cryo_overflow_workspace/repo/cryo-overflow')

    if type == 'snippet':
        # Construct the destination path
        dest_path = os.path.join(repo.working_tree_dir, 'snippets', data['language'])
        
        # Make sure the destination directory exists
        os.makedirs(dest_path, exist_ok=True)
        
        # Move the markdown file to the destination directory
        if os.path.exists(os.path.join(dest_path, os.path.basename(markdown_path))):
            new_path = os.path.join(dest_path, os.path.basename(markdown_path).replace('.md', '_1.md'))
            shutil.move(markdown_path, new_path)
        else:
            shutil.move(markdown_path, dest_path)
            

        # Construct the path relative to the repository root
        rel_path = os.path.relpath(dest_path, start=repo.working_tree_dir)

        # Stage the changes
        repo.git.add(rel_path)
        
    elif type == 'file':
        # Construct the destination path
        dest_path = os.path.join(repo.working_tree_dir, 'programs', data['language'], data['title'].replace(' ', '_'))
        
        # Make sure the destination directory exists
        os.makedirs(dest_path, exist_ok=True)

        # Move the markdown file to the destination directory
        if os.path.exists(os.path.join(dest_path, os.path.basename(markdown_path))):
            new_path = os.path.join(dest_path, os.path.basename(markdown_path).replace('.md', '_1.md'))
            shutil.move(markdown_path, new_path)
        else:
            shutil.move(markdown_path, dest_path)

        # Move the uploaded file to the destination directory
        uploads_folder = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/uploads/'
        input_file_path = os.path.join(uploads_folder, data['file'].filename)
        shutil.move(input_file_path, dest_path)

        # Construct the path relative to the repository root
        rel_path = os.path.relpath(dest_path, start=repo.working_tree_dir)

        # Stage the changes
        repo.git.add(rel_path)

    elif type == 'blog':
        # Construct the destination path
        dest_path = os.path.join(repo.working_tree_dir, "guidelines")

        # Make sure the destination directory exists
        os.makedirs(dest_path, exist_ok=True)

        # Move the markdown file to the destination directory
        if os.path.exists(os.path.join(dest_path, os.path.basename(markdown_path))):
            new_path = os.path.join(dest_path, os.path.basename(markdown_path).replace('.md', '_1.md'))
            shutil.move(markdown_path, new_path)
        else:
            shutil.move(markdown_path, dest_path)

        # Construct the path relative to the repository root
        rel_path = os.path.relpath(dest_path, start=repo.working_tree_dir)

        # Stage the changes
        repo.git.add(rel_path)

    # Commit the changes
    repo.index.commit('Add new submission')

    # Push the changes to the remote repository
    origin = repo.remote(name='origin')
    origin.push()


