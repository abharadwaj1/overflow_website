from git import Repo
import os
import shutil
import fnmatch

def search_matching_files(query):
    repo = Repo('/home/abharadwaj1/dev/cryo_overflow_workspace/repo/cryo-overflow')
    main_repo_folder = repo.working_tree_dir
    matches = []    
    print("Query: " + query)
    print("Main repo folder: " + main_repo_folder)
    for root, dirnames, filenames in os.walk(main_repo_folder):
        print("Root: " + root)
        for filename in fnmatch.filter(filenames, '*.md'):
            print("Filename: " + filename)
            with open(os.path.join(root, filename), 'r') as file:
                content = file.read()
                print(type(content), type(query), type(filename))
                if query.lower() in content.lower():
                    # get context within file
                    line_containing_query = 0 
                    for line in content.splitlines():
                        if query.lower() in line.lower():
                            break
                        line_containing_query += 1
                    number_of_lines = len(content.splitlines())
                    if number_of_lines < 6:
                        context = content
                    else:
                        lines_before = min(3, line_containing_query)
                        lines_after = min(3, len(content.splitlines()) - line_containing_query)
                        context = '\n'.join(content.splitlines()[line_containing_query - lines_before : line_containing_query + lines_after + 1])
                                        
                    repo_url_main = "https://gitlab.tudelft.nl/aj-lab/cryo-overflow/-/blob/main/"
                    location_within_repo = os.path.relpath(root, start=main_repo_folder)
                    print("Location within repo: " + location_within_repo)
                    filename_within_repo = os.path.join(repo_url_main,location_within_repo, filename)
                    matches_dict = {
                        "filename" : filename,
                        "context" : context,
                        "file_link" : filename_within_repo,
                    }
                    matches.append(matches_dict)
    return matches
