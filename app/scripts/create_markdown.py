import os

def create_markdown(data, type, file_path=None):
    markdown_folder = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/markdown/'
    markdown_path = os.path.join(markdown_folder, data['title'].replace(' ', '_') + '.md')
    
    with open(markdown_path, 'w') as f:
        f.write(f"# {data['title']}\n")
        if "description" in data:
            f.write(f"## Description\n{data['description']}\n")
        
        
        if type == 'snippet':
            f.write(f"## Code\n```{data['language']}\n{data['code']}\n```\n")
        elif type == 'file':
            with open(file_path, 'r') as code_file:
                f.write(f"## Code\n```{data['language']}\n{code_file.read()}\n```\n")
        elif type == 'blog':
            f.write(f"## Post\n{data['post']}\n")
        
        if 'image' in data and data['image'] is not None:
            image_path = os.path.join('images', data['image'].filename)
            data['image'].save(image_path)
            f.write(f"![{data['title']}]({image_path})\n")
            
    return markdown_path
